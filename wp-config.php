<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'temp-vtj');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin@pwddb1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't!E#U4siKHD>>TaKUT])5R>6<=cZAHkEZO30L$[>Lz8Pok.yk_Pme9O`1W5qR]4[');
define('SECURE_AUTH_KEY',  '%oe2ABinm*Ml|&0EB`:YhynY101a=$-&0S^6Q,y^IY.9vYEnPr,Pq=I 3jb6QFx1');
define('LOGGED_IN_KEY',    '?N^puEK3baXX.IO%KNyQrZ%NQ,v8_Sz8;qQk6Bco$E{:97Z*z:QC6V$:v6DYFa(u');
define('NONCE_KEY',        'Mon$lwONj.HOYlWb<kOCai@b4o(FjBlLi,FWD;lseSbB9?H-Awd+E8rUwO#/;wnD');
define('AUTH_SALT',        '2#CDGvJDpK;q*W.X#kS8VR)bCof)Frw3Yz)ZZ1z8X~*^;bX]wyX9a+HFX&ros(h=');
define('SECURE_AUTH_SALT', '8JR!Z:Z!]$]!1%L>UCVk6o|dFc<!300#%%xc9(*@xIiF/$jm%LjsQ}BvOkA$G*-s');
define('LOGGED_IN_SALT',   'h%6cgRUX)s,^!2??h09cma)$ktA(`+CKW$/8%0le+51s{WSliF[g2Wn~-v5/p;M7');
define('NONCE_SALT',       'uVX%w@S;T%95TCKrxn5~yE;.>3Xs*@z9x@@1U1L<l/_d{,{iSk>$Nagq|j%pSSEG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
